﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore.Models
{
    public class InfectedFile
    {
        public InfectedFile(string fileName, string virusName)
        {
            this.FileName = fileName;
            this.VirusName = virusName;
        }

        public string FileName { get; }

        public string VirusName { get; }
    }
}