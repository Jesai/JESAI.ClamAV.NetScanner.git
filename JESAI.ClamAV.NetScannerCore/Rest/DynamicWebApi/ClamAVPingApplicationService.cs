﻿using JESAI.DynamicWebApi;
using JESAI.DynamicWebApi.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JESAI.ClamAV.NetScannerCore.Rest.DynamicWebApi
{
    [DynamicWebApi]
    public class ClamAVPingApplicationService : IDynamicWebApiService
    {
        private readonly IClamAVScannerClient ClamAVScannerClient;
        public ClamAVPingApplicationService(IClamAVScannerClient clamAVScannerClient)
        {
            ClamAVScannerClient = clamAVScannerClient;
        }
        /// <summary>
        /// 获取ClamAV服务器版本
        /// </summary>
        public Task<bool> GetPingAsync()
        {
            return ClamAVScannerClient.PingAsync();
        }
    }
}
