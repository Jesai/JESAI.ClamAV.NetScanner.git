﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore.Enums
{
    public enum ClamAVCommand
    {
        PING,
        VERSION,
        RELOAD,
        SHUTDOWN,
        SCAN,  //SCAN 文件/目录
        RAWSCAN,//RAWSCAN 文件/目录
        CONTSCAN,//CONTSCAN 文件/目录
        MULTISCAN,//MULTISCAN 文件/目录
        ALLMATCHSCAN,//ALLMATCHSCAN 文件/目录
        INSTREAM,
        FILDES,
        STATS,
        IDSESSION,
        END,
        SIGTERM,//SIGTERM -执行干净的出口
        SIGHUP,    //SIGHUP -重新打开日志文件
        SIGUSR2         //SIGUSR2 -重新加载数据库
    }
}
