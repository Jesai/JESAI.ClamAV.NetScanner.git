﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace JESAI.ClamAV.NetScannerCore.TcpClients
{
    public interface IScannerTCPClient
    {
        int ReceiveTimeout { get; set; }
        int ReceiveBufferSize { get; set; }
        bool NoDelay { get; set; }
        LingerOption LingerState { get; set; }
        bool ExclusiveAddressUse { get; set; }
        bool Connected { get; }
        Socket Client { get; set; }
        int Available { get; }
        int SendBufferSize { get; set; }
        int SendTimeout { get; set; }
        bool Active { get; set; }
        IAsyncResult BeginConnect(string host, int port, AsyncCallback requestCallback, object state);
        IAsyncResult BeginConnect(IPAddress[] addresses, int port, AsyncCallback requestCallback, object state);
        IAsyncResult BeginConnect(IPAddress address, int port, AsyncCallback requestCallback, object state);
        void Close();      
        void Connect(string hostname, int port);      
        void Connect(IPEndPoint remoteEP);
        void Connect(IPAddress[] ipAddresses, int port);
        void Connect(IPAddress address, int port);       
        Task ConnectAsync(IPAddress[] addresses, int port);
        Task ConnectAsync(IPAddress address, int port);
        Task ConnectAsync(string host, int port);
        void Dispose();
        void EndConnect(IAsyncResult asyncResult);
        NetworkStream GetStream();
    }
}
