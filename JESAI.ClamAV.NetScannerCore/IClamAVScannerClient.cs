﻿using JESAI.ClamAV.NetScannerCore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JESAI.ClamAV.NetScannerCore
{
    public interface IClamAVScannerClient
    {      
        /// <summary>
        /// 获取ClamAV服务器版本
        /// </summary>
        Task<string> GetVersionAsync();

        /// <summary>
        /// 获取ClamAV服务器版本
        /// </summary>
        /// <param name="cancellationToken">用于请求的取消令牌</param>
        Task<string> GetVersionAsync(CancellationToken cancellationToken);

        /// <summary>
        /// 在ClamAV服务器上执行PING命令。
        /// </summary>
        /// <returns>如果服务器以PONG响应，则返回true。否则返回false。</returns>
        Task<bool> PingAsync();

        /// <summary>
        /// 在ClamAV服务器上执行PING命令。
        /// </summary>
        /// <param name="cancellationToken">用于请求的取消令牌</param>
        /// <returns>如果服务器以PONG响应，则返回true。否则返回false。</returns>
        Task<bool> PingAsync(CancellationToken cancellationToken);

        /// <summary>
        /// 扫描ClamAV服务器上的文件/目录。
        /// </summary>
        /// <param name="filePath">ClamAV服务器上文件/目录的路径。</param>
        Task<ScannerResult> ScanFileOnServerAsync(string filePath);

        /// <summary>
        /// 扫描ClamAV服务器上的文件/目录。
        /// </summary>
        /// <param name="filePath">ClamAV服务器上文件/目录的路径。</param>
        /// <param name="cancellationToken">用于请求的取消令牌</param>
        Task<ScannerResult> ScanFileOnServerAsync(string filePath, CancellationToken cancellationToken);

        /// <summary>
        /// 使用服务器上的多个线程扫描ClamAV服务器上的文件/目录。
        /// </summary>
        /// <param name="filePath">ClamAV服务器上文件/目录的路径。</param>
        Task<ScannerResult> ScanFileOnServerMultithreadedAsync(string filePath);

        /// <summary>
        /// 使用服务器上的多个线程扫描ClamAV服务器上的文件/目录。
        /// </summary>
        /// <param name="filePath">ClamAV服务器上文件/目录的路径。</param>
        /// <param name="cancellationToken">用于请求的取消令牌</param>
        Task<ScannerResult> ScanFileOnServerMultithreadedAsync(string filePath, CancellationToken cancellationToken);

        /// <summary>
        /// 将数据作为流发送到ClamAV服务器。
        /// </summary>
        /// <param name="fileData">包含文件数据的字节数组。</param>
        /// <returns></returns>
        Task<ScannerResult> SendAndScanFileAsync(byte[] fileData);
        /// <summary>
        /// 将数据作为流发送到ClamAV服务器。
        /// </summary>
        /// <param name="fileData">包含文件数据的字节数组。</param>
        /// <returns></returns>
        Task<ScannerResult> SendAndScanBytesAsync(byte[] bytes);

        /// <summary>
        /// 将数据作为流发送到ClamAV服务器。
        /// </summary>
        /// <param name="fileData">包含文件数据的字节数组。</param>
        /// <param name="cancellationToken">用于请求的取消令牌</param>
        /// <returns></returns>
        Task<ScannerResult> SendAndScanFileAsync(byte[] fileData, CancellationToken cancellationToken);

        /// <summary>
        /// 将数据作为流发送到ClamAV服务器。
        /// </summary>
        /// <param name="sourceStream">包含要扫描的数据的流。</param>
        /// <returns></returns>
        Task<ScannerResult> SendAndScanFileAsync(Stream sourceStream);

        /// <summary>
        /// 将数据作为流发送到ClamAV服务器。
        /// </summary>
        /// <param name="sourceStream">包含要扫描的数据的流。</param>
        /// <param name="cancellationToken">用于请求的取消令牌</param>
        /// <returns></returns>
        Task<ScannerResult> SendAndScanFileAsync(Stream sourceStream, CancellationToken cancellationToken);

        /// <summary>
        /// 从路径读取文件，然后将其作为流发送到ClamAV服务器。
        /// </summary>
        /// <param name="filePath">文件/目录的路径。</param>
        Task<ScannerResult> SendAndScanFileAsync(string filePath);

        /// <summary>
        /// 从路径读取文件，然后将其作为流发送到ClamAV服务器。
        /// </summary>
        /// <param name="filePath">文件/目录的路径。</param>
        /// <param name="cancellationToken">用于请求的取消令牌</param>
        Task<ScannerResult> SendAndScanFileAsync(string filePath, CancellationToken cancellationToken);
    }
}
