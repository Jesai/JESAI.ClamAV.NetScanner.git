﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore.Exceptions
{
    public class TCPServerException : Exception
    {
        public TCPServerException(string host, int port,Exception ex) : base($"无法访问ClamAV服务器 {host}:{port};Message:" +ex.Message+ ";InnerException:"+ex.InnerException)
        {

        }
    }
}
