﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore.Exceptions
{
    public class ClamAVScannerResultEmptyOrNullException:Exception
    {
        public ClamAVScannerResultEmptyOrNullException() : base("原始扫描结果不能为空。")
        {
        }
    }
}
