﻿using JESAI.ClamAV.NetScannerCore.Command;
using JESAI.ClamAV.NetScannerCore.Configuration;
using JESAI.ClamAV.NetScannerCore.Rest.DynamicWebApi;
using JESAI.ClamAV.NetScannerCore.TcpClients;
using JESAI.ClamAV.NetScannerCore.TcpClients.Builder;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore.DependencyInjection
{
    public static class ClamAVNetScannerServiceCollectionExtensions
    {
        public static IServiceCollection AddClamAVNetScanner(this IServiceCollection services, IClamAvScannerConfiguration clamAvScannerConfiguration=null)
        {

            services.AddConfiguration(clamAvScannerConfiguration);
            RegisterServices(services);
            return services;
        }

        public static IServiceCollection AddClamAVNetScanner(this IServiceCollection services, Action<IServiceCollection> action = null)
        {
            action?.Invoke(services);
            RegisterServices(services);
            return services;
        }

        private static void RegisterServices(IServiceCollection services)
        {
            services.AddTransient<IScannerTcpClientBuilder, TransientScannerTcpClientBuilder>();
            services.AddSingleton<ICommandBuilder, CommandBuilder>();
            services.AddTransient<ICommandExecutor, CommandExecutor>();
            services.AddTransient<IClamAVScannerClient, ClamAVScannerClient>();
            services.AddSingleton<ClamAVVerionsApplicationService>();
            // 注册Swagger生成器，定义一个和多个Swagger 文档
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo() { Title = "JESAI.ClamAV.NetScanner.Demo", Version = "v1" });

                // TODO:一定要返回true！
                options.DocInclusionPredicate((docName, description) =>
                {
                    return true;
                });
                //options.IncludeXmlComments(AppDomain.CurrentDomain.BaseDirectory + @"\JESAI.ClamAV.NetScanner.Demo.xml");
                options.IncludeXmlComments(AppDomain.CurrentDomain.BaseDirectory + @"\JESAI.ClamAV.NetScannerCore.xml");
            });
        }

        private static void AddConfiguration(this IServiceCollection services, IClamAvScannerConfiguration clamAvScannerConfiguration)
        {
            if (clamAvScannerConfiguration != null)
            {
                services.AddSingleton<IClamAvScannerConfiguration>(clamAvScannerConfiguration);
            }
            else
            {
                services.AddSingleton<IClamAvScannerConfiguration, ClamAvScannerConfiguration>();
            }
        }
        public static IServiceCollection AddClamAVNetScannerDynamicWebApi(this IServiceCollection services)
        {
            services.AddDynamicWebApi(Assembly.GetExecutingAssembly());
            return services;
        }
            public static IApplicationBuilder UseClamAVNetScanner(this IApplicationBuilder app)
        {
            //启用中间件服务生成Swagger作为JSON终结点
            app.UseSwagger();
            //启用中间件服务对swagger-ui，指定Swagger JSON终结点
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "JESAI.ClamAV.NetScanner.Demo");
            });
            return app;
        }

    }
}
