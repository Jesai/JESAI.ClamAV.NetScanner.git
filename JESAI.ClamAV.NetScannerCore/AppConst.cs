﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JESAI.ClamAV.NetScannerCore
{
    public struct AppConst
    {
        public const string Host="127.0.0.1";
        public const int Port = 3310;
        public const int MaxChunkSize = 2048;
        public const int MaxStreamSize = 26214400;

    }
}
