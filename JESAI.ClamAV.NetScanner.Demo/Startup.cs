using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using JESAI.ClamAV.NetScannerCore;
using JESAI.ClamAV.NetScannerCore.Configuration;
using JESAI.ClamAV.NetScannerCore.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace JESAI.ClamAV.NetScanner.Demo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //IClamAvScannerConfiguration不配置的话，会默认
            //Host = "127.0.0.1",
            //Port = 3310,
            //MaxChunkSize = 2014,
            //MaxStreamSize = 26214400
            IClamAvScannerConfiguration configuration = new ClamAvScannerConfiguration()
            {
                Host="127.0.0.1",
                Port=3310,
                MaxChunkSize=2014,
                MaxStreamSize= 26214400
            };
            services.AddClamAVNetScanner(configuration);
            //也可以这样用
            //services.AddClamAVNetScanner((s) => { s.AddSingleton<IClamAvScannerConfiguration>(configuration); });
            services.AddControllersWithViews();
            services.AddClamAVNetScannerDynamicWebApi();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseClamAVNetScanner();
        }
    }
}
